
import { Router } from 'express';
import { Address } from '../interfaces/address.interfaces';

const addresses: Address[] = [
    {
        id: 1,
        street: 'Długa 3',
        city: 'Bydgoszcz',
        postcode: '85-034'
    }
]

export const addressRoutes = Router()

addressRoutes.get<null, Address[], null, null>('/', (req, res) => {
    res.send(addresses)
})
addressRoutes.get<any, Address, null, null>('/:id', (req, res) => {
    res.send(addresses.filter(x => x.id == req.params.id).shift())
})