
import { Router } from 'express';
import { UserListResponse, UserQueryParams, UserDTOs } from '../interfaces/user.interfaces';

export const userRoutes = Router()


userRoutes.get<{}, UserListResponse, null, UserQueryParams>('/', (req, res) => {
  let { filter/* , limit = '10', offset = '0' */ } = req.query
  const limit = parseInt(req.query.limit || '10')
  const offset = parseInt(req.query.offset || '10')

  const userResponse = {
    items: [
      {
        id: '123',
        name: 'User 243'
      },
      {
        id: '123',
        name: 'User 123'
      },
    ],
    filter,
    limit,
    offset,
  };

  res.send(userResponse)
})


userRoutes.get<null, UserDTOs.User, null, null>('/me', (req, res) => {
  res.send({
    id: '123', name: '123'
  })
})

userRoutes.get<{ id: string }, UserDTOs.User>("/:id", function (req, res) {
  res.send({
    id: '123', name: '123'
  });
});



userRoutes.get('/calculate', (req, res) => {

  const date = Date.now() + 10_000
  // while (Date.now() < date) { }

  const handle = setInterval(() => {
    console.log('working hard...')

    if (Date.now() > date) {
      clearInterval(handle)
      res.end('<h1>Bye bye! </h1>')
    } else {
      res.flushHeaders()
      res.write('<h1>Hello you </h1>')
    }
  }, 0)

})

